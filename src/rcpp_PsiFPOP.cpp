#include <Rcpp.h>
#include "PsiFPOP.h"
#include "PsiOP.h"
#include "PsiPELT.h"
#include "PsiOPV2.h"
#include <string>
#include <vector>
#include "Sampling.h"
#include <map>
using namespace Rcpp;

// [[Rcpp::export]]
List PsiFPOP_cpp(
  std::vector<double> y, 
  double beta, 
  double alpha, 
  std::string sampling_method, 
  int sampling_method_parameter, 
  std::vector<double>  wt) {   

    typedef std::vector<int> (*fun)(int, int, int);
    std::map<std::string, fun> map_sampling_method;
    map_sampling_method["rand_one"]                 = Sampling::Rand_one;
    map_sampling_method["last"]                     = Sampling::Last;
    map_sampling_method["all"]                      = Sampling::All;
    map_sampling_method["only"]                     = Sampling::Only;
    map_sampling_method["rand_with_replacement"]    = Sampling::Rand_with_replacement;
    map_sampling_method["rand_without_replacement"] = Sampling::Rand_without_replacement;
    
    if (map_sampling_method.find(sampling_method)!= map_sampling_method.end())
    {
      PsiFPOP f = PsiFPOP(
        y, 
        beta, 
        alpha, 
        (*map_sampling_method[sampling_method]), 
        sampling_method_parameter,
        wt
      );
      f.Search();
      List l = List::create(
    	_["changepoints"] = f.Retreive_changepoints(),
      _["intervals"]    = f.Get_intervals(),
      _["candidates"]   = f.Get_candidates()
      );
      return l;
    }
    else
    { 
      return List ();
    }
}

// [[Rcpp::export]]
List PsiOP_cpp(
  std::vector<double> y, 
  double beta, 
  double alpha, 
  std::vector<double>  wt) {
  
  PsiOP f (y, beta, alpha, wt);
  f.Search();
  List l = List::create(
    _["changepoints"] = f.Retreive_changepoints()
  );
  return l;
}


//- All pelt inequality constants are defined in PeltPruningCst but leads to -//
//- weird compilation error ... Until error fixed define constants below : ---//
double cst0(int l1) {
  return 0.0;
} 

double cst1(int l1) {
  return std::log(l1+1);
} 

double cst2(int l1) {
  return std::log(2*l1);
}

// [[Rcpp::export]]
List PsiPELT_cpp(
  std::vector<double> y, 
  double beta, 
  double alpha, 
  std::string K, 
  std::vector<double> wt) {
  
  typedef double (*fun)(int);
  std::map<std::string, fun> map_K;
  map_K["cst0"] = cst0;
  map_K["cst1"] = cst1;
  map_K["cst2"] = cst2;
  
  PsiPELT f (y, beta, alpha, wt, (*map_K[K]));
  //PsiPELT f (y, beta, alpha, wt);
  f.Search();
  List l = List::create(
    _["changepoints"] = f.Retreive_changepoints(),
    _["candidates"]   = f.Get_candidates()
  );
  return l;
}


// [[Rcpp::export]]
List PsiOPV2_cpp(
    std::vector<double> y, 
    double beta, 
    double alpha, 
    std::vector<double>  wt) {
  
  PsiOPV2 f (y, beta, alpha, wt);
  f.Search();
  List l = List::create(
    _["changepoints"] = f.Retreive_changepoints()
  );
  return l;
}
