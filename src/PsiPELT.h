#ifndef DEF_PSIPELT
#define DEF_PSIPELT

#include <vector>

class PsiPELT
{
private:
    std::vector<double> y;
    int n;
    double beta;
    double alpha;
    std::vector<double> wt;
    std::vector<int> cp;
    std::vector<double> K;
    std::vector<double> css;
    std::vector<double> cs;
    std::vector<double> Q;
    std::vector<int> nb_candidates;
    double (*pruningCst)(int);
    
public:
    /**
     * @param[in] y A vector of observations.
     * @param[in] beta a constant used in the calculation of the penalty.
     * @param[in] alpha a constant used in the calculation of the penalty.
     * @param[in] wt A vector of weights associated to the observations.   
     */
    PsiPELT(
        std::vector<double> y_, 
        double beta_, double alpha_, 
        std::vector<double> wt_,
        double (*pruningCst_)(int)
    );
    
    /**
* @details Procedure to infer the number of changepoints and their location in the data.
     */
    void Search();

    /**
     * @returns the location of changepoints in the data.  
     */
    std::vector<int> Retreive_changepoints();
    /**
     * @returns A vector that contains the total number of candidates before the pruning step at each iteration.
     */
    std::vector<int> Get_candidates();
};


#endif