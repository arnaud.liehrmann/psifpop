#ifndef DEF_PSIOPV2
#define DEF_PSIOPV2

#include <vector>

class PsiOPV2
{
private:
    std::vector<double> y;
    int n;
    double beta;
    double alpha;
    std::vector<double> wt;
    std::vector<int> cp;
    std::vector<double> K;
    std::vector<double> css;
    std::vector<double> cs;
    std::vector<double> Q;
    
public:
    /**
     * @param[in] y A vector of observations.
     * @param[in] beta a constant used in the calculation of the penalty.
     * @param[in] alpha a constant used in the calculation of the penalty.
     * @param[in] wt A vector of weights associated to the observations.   
     */
    PsiOPV2(
        std::vector<double> y_, 
        double beta_, double alpha_, 
        std::vector<double> wt_
    );
    
    /**
* @details Procedure to infer the number of changepoints and their location in the data.
     */
    void Search();

    /**
     * @returns the location of changepoints in the data.  
     */
    std::vector<int> Retreive_changepoints();
};


#endif