#ifndef DEF_PELTPRUNINGCST
#define DEF_PELTPRUNINGCST

class PeltPruningCst
{
private:
public:
  static double cstA(int l1);
  static double cstB(int l1);
  static double cstC(int l1);
  static double cstD(int l1);
};

#endif