# PsiFPOP
### Arnaud Liehrmann

> [Introduction](#intro)

> [Installation Notes](#instal)


<a id="intro"></a>

## Introduction

KEEP THIS VERSION FOR REPRODUCIBILITY OF THE PAPER.

THE USER VERSION IS AVAILABLE [HERE](https://forgemia.inra.fr/arnaud.liehrmann/msfpop)


<a id="instal"></a>

## Installation Notes 

#### Step 1: Install the devtools package

```
install.packages("devtools")
```

#### Step 2: Install the `PsiFPOP` package from GitHub

```
library(devtools)
install_git(url = "https://forgemia.inra.fr/arnaud.liehrmann/psifpop")
```

#### Step 3: Load the package

```
library(PsiFPOP)
```
